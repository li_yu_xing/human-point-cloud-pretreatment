#pragma once
#include"config.h"
//typedef pcl::PointXYZ					Point;
//typedef pcl::PointXYZ					myPoinT;
//typedef pcl::PointXYZRGB				myPoinT_RGB;
//typedef pcl::PointNormal				PoinTNormal;
//typedef pcl::PointCloud<myPoinT>		PointCloud;   //申明pcl::PointXYZ数据
//typedef pcl::PointCloud<myPoinT_RGB>	PointCloud_RGB;   //申明pcl::PointXYZRGB数据
//pcl::PointCloud<pcl::PointXYZ>        PointCloud

#define USING_SHOW_CLOUD			1
#define _MIN3(a,b,c)		(a < b ? (a<c ? a:c) : (b<c ? b:c))
class Common
{
public:
	//显示点云
	static void				ShowCloud(std::string title, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
	//显示点云2
	static void				ShowCloud2(std::string title, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2);
	//降采样滤波 均匀采样
	static void             filter_US(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, float radius);
	//降采样滤波 体素滤波
	static void				filter_VoxelGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, float leafSize);
	//平滑处理
	static void				process_mls(pcl::PointCloud<pcl::PointXYZ>::Ptr & in, pcl::PointCloud<pcl::PointXYZ>::Ptr & out, double radius);
	//高斯滤波
	static void				filter_Gaussian(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, double radius);
	//双边滤波
	static void				filter_bilateral2(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloudout, float sigma_s, float sigma_r);

	//直通滤波
	static void				filter_pass_through(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud_filtered2, double z1, double z2, double x1, double x2, double y1, double y2);
	//删除平面
	static bool				process_removePlane(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, const float dis_threshold, const int max_iterations);
	//中心点平移到中点
	static void				MoveCenTo000(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out);
	//旋转喝平移
	static void				RotateA(pcl::PointCloud<pcl::PointXYZ> input, double A, double B, double C, pcl::PointCloud<pcl::PointXYZ> &transformed_cloud);
	//点云地面处理
	static void				process_estimatePlane(pcl::PointCloud<pcl::PointXYZ>::Ptr &in_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &out_cloud, const float dis_threshold, const int max_iterations, const int showcloud);
	//半径滤波器RadiusOutlierRemoval
	static void				filter_ROR(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out);
	//统计滤波器
	static void				filter_SOR(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out);
	//边界估计
	static void				estimateBorders(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_boundary, float reforn);
	//计算点云的平均距离
	static float			avg_distance(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);

	//点云投影到XY平面
	static void				process_TransPInliers(pcl::PointCloud<pcl::PointXYZ>::Ptr & in, pcl::PointCloud<pcl::PointXYZ>::Ptr & out);
	//X-Y排序
	static void				PointCloudSortXY(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud);
	//算数平均滤波法
	static void				run_avg_fil(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, int k_or_radius);

	static void				BodyDate(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
	static void				savePCDFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
	static void				savePLYFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
	//B线条曲边拟合
	static void				run_curv_fitting(pcl::PointCloud<pcl::PointXYZ>::Ptr&cloud);
#if USING_BILATERAL
	//双边滤波
	static PointCloud::Ptr	filter_bilateral1(PointCloud::Ptr& cloud, float sigma_s, float sigma_r);
#endif

	//估计法向量
	static void				estimateNormal(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
	static void				RotateB(pcl::PointCloud<pcl::PointXYZ> input, double A, double B, double C, pcl::PointCloud<pcl::PointXYZ> &transformed_cloud);
	//根据向量，计算旋转矩阵
	static Eigen::Matrix4f  CalcRotatedMatrix(Eigen::Vector3f before, Eigen::Vector3f after);

	//表面重建
	static void				process_UpdateSurface(pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud);
private:

};

struct G_CFG {

	//是否显示点云
	int		showcloud = 0;
	//是否需要移除前面
	int		removewall = 1;
	//最小人体点云数
	int		minBodySize = 500;
	//最大人体点云数
	int		maxBodySize = 3000;

	//降采样滤波叶大小mm（为0时关闭功能）
	float	LEAFSIZE = 10;
	//平滑滤波半径mm（为0时关闭功能）
	double	RADIUS_MLS = 50;
	//高斯滤波半径mm（为0时关闭功能）
	double	RADIUS_GAUSSIAN = 50;

	//直通滤波范围
	double	rang_z1 = 1800;
	double	rang_z2 = 3200;
	double	rang_x1 = -660;
	double	rang_x2 = 1000;
	double	rang_y1 = -1200;
	double	rang_y2 = 660;

	//墙面点云计算
	float	dis_threshold1 = 30;
	int		max_iterations1 = 16000;
	//地面点云计算
	float	dis_threshold2 = 30;
	int		max_iterations2 = 16000;


};