#include "Common.h"
//显示点云
void Common::ShowCloud(std::string title, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud) {
#if USING_SHOW_CLOUD
	// 初始化点云可视化对象
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer(title));
	// 设置背景颜色为黑色
	viewer->setBackgroundColor(0, 0, 0);
	// 对点云着色可视化
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>target_color(cloud, 255, 255, 255);
	// 把点云加载到视窗
	viewer->addPointCloud<pcl::PointXYZ>(cloud, target_color, "ShowCloud");
	// 设置点云大小
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "ShowCloud");
	// 等待直到可视化窗口关闭
	while (!viewer->wasStopped())
	{
		viewer->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(500));
	}

	//viewer->spinOnce(1000000);

#endif
}
//显示两个点云
void Common::ShowCloud2(std::string title, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2)
{
#if USING_SHOW_CLOUD
	// 以下都是可视化部分
	//pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer(title));
	pcl::visualization::PCLVisualizer viewer(title);
	int v1(0);
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.addCoordinateSystem(v1);
	viewer.setBackgroundColor(0, 0, 0, v1);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color(cloud1, 0, 255, 0);
	viewer.addPointCloud(cloud1, color, "cloud1", v1);

	int v2(0);
	viewer.createViewPort(0.5, 0.0, 1.0, 1.0, v2);
	viewer.addCoordinateSystem(v2);
	viewer.setBackgroundColor(0, 0, 0, v2);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color_transformed(cloud2, 0, 255, 0);
	viewer.addPointCloud(cloud2, color_transformed, "cloud2", v2);

	while (!viewer.wasStopped()) {
		viewer.spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
#else
	//可视化
	pcl::visualization::PCLVisualizer viewer(title);
	// 创建两个观察视点
	int v1(0);
	int v2(1);
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.createViewPort(0.5, 0.0, 1.0, 1.0, v2);

	// 设置背景颜色为黑色
	viewer.setBackgroundColor(0, 0, 0, v1);
	viewer.setBackgroundColor(0, 0, 0, v2);

	// 对点云着色可视化
	pcl::visualization::PointCloudColorHandlerCustom<myPoinT> target_color1(cloud1, 255, 255, 255);
	pcl::visualization::PointCloudColorHandlerCustom<myPoinT> target_color2(cloud2, 255, 255, 255);
	// 把点云加载到视窗
	viewer.addPointCloud<myPoinT>(cloud1, target_color1, "ShowCloud_v1", v1);
	viewer.addPointCloud<myPoinT>(cloud2, target_color2, "ShowCloud_v2", v2);
	// 设置点云大小
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "ShowCloud_v1", v1);
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "ShowCloud_v2", v2);
	// 等待直到可视化窗口关闭
	while (!viewer.wasStopped())
	{
		viewer.spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(500));
	}
#endif
}

//降采样滤波 均匀采样
void Common::filter_US(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_out, float radius)
{
	/********************************************************************************************/
	//PointCloud::Ptr cloud_out(new PointCloud);
	pcl::UniformSampling<pcl::PointXYZ> us;	//创建滤波对象
	us.setInputCloud(cloud);			//设置需要过滤的点云给滤波对象
	us.setRadiusSearch(radius);			//设置滤波时创建的球体半径
	us.filter(*cloud_out);				//执行滤波处理，存储输出
	//return cloud_out;
	/********************************************************************************************/
}


//降采样滤波 体素滤波
void Common::filter_VoxelGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud1, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_out, float leafSize)
{
	/*******************************************************************************************/
	//降采样：创建一个voxel叶大小为5mm的pcl::VoxelGrid滤波器，
	/********************************************************************************************/
	//PointCloud::Ptr cloud_out(new PointCloud);
	pcl::VoxelGrid<pcl::PointXYZ> vg;			//创建滤波对象
	vg.setInputCloud(cloud1);			//设置需要过滤的点云给滤波对象
	vg.setLeafSize(leafSize, leafSize, leafSize);	//设置滤波时创建的体素体积为leafSize^3的立方体
	vg.filter(*cloud_out);				//执行滤波处理，存储输出
	/********************************************************************************************/
	//return cloud_out;
}


//滑动最小二乘法
void Common::process_mls(pcl::PointCloud<pcl::PointXYZ>::Ptr & in, pcl::PointCloud<pcl::PointXYZ>::Ptr & out, double radius)
{
	//创建搜索树
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud(in);

	//最小二乘
	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;//定义最小二乘对象
	mls.setComputeNormals(false);	//设置最小二乘计算是否需要存储计算的法线
	mls.setInputCloud(in);			//设置输入点云
	mls.setPolynomialOrder(2);		//2阶多项式
	mls.setPolynomialFit(false);	//设置false 可以加速smooth
	mls.setSearchMethod(tree);		//设置kdTree作为搜索方法
	mls.setSearchRadius(radius);	//拟合的k临近半径
	mls.process(*out);
}



//高斯滤波
void Common::filter_Gaussian(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, double radius)
{
	////////////////////////////////////高斯滤波//////////////////////////////////////
	//Set up the Gaussian Kernel
	pcl::filters::GaussianKernel<pcl::PointXYZ, pcl::PointXYZ>::Ptr kernel(new pcl::filters::GaussianKernel<pcl::PointXYZ, pcl::PointXYZ>);
	(*kernel).setSigma(8);
	(*kernel).setThresholdRelativeToSigma(5);
	std::cout << "Kernel made" << std::endl;

	//Set up the KDTree
	pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);
	(*kdtree).setInputCloud(cloud);
	std::cout << "KdTree made" << std::endl;

	//Set up the Convolution Filter
	pcl::filters::Convolution3D<pcl::PointXYZ, pcl::PointXYZ, pcl::filters::GaussianKernel<pcl::PointXYZ, pcl::PointXYZ>> convolution;
	convolution.setKernel(*kernel);
	convolution.setInputCloud(cloud);
	convolution.setSearchMethod(kdtree);
	convolution.setRadiusSearch(radius);
	std::cout << "Convolution Start" << std::endl;

	convolution.convolve(*cloud_out);
	std::cout << "Convoluted" << std::endl;
}

//双边滤波
void Common::filter_bilateral2(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out, float sigma_s, float sigma_r)
{
	// Apply the filter 使用傅立叶近似的快速双边滤波
	pcl::FastBilateralFilter<pcl::PointXYZ>::Ptr fbf(new pcl::FastBilateralFilter<pcl::PointXYZ>);
	fbf->setInputCloud(cloud);
	fbf->setSigmaS(sigma_s);//设置双边滤波器用于空间邻域/窗口的高斯的标准偏差
	fbf->setSigmaR(sigma_r);//设置高斯的标准偏差用于控制相邻像素由于强度差异而下降多少
	fbf->filter(*cloud_out);
}


//直通滤波
void Common::filter_pass_through(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_filtered2, double z1, double z2, double x1, double x2, double y1, double y2)
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered1(new pcl::PointCloud<pcl::PointXYZ>);

	/************************************************************************************
	 创建直通滤波器的对象，设立参数，滤波字段名被设置为Z轴方向，可接受的范围为（0.0，1.0）
	 即将点云中所有点的Z轴坐标不在该范围内的点过滤掉或保留，这里是过滤掉，由函数setFilterLimitsNegative设定
	 ***********************************************************************************/
	 //设置滤波器对象
	pcl::PassThrough<pcl::PointXYZ> pass;
	pass.setFilterLimitsNegative(false);  //设置保留范围内还是过滤掉范围内

	pass.setInputCloud(cloud);				//设置输入点云
	pass.setFilterFieldName("z");			//设置过滤时所需要点云类型的Z字段
	pass.setFilterLimits(z1, z2);			//设置在过滤字段的范围
	pass.filter(*cloud_filtered);          //执行滤波，保存过滤结果在cloud_filtered

	pass.setInputCloud(cloud_filtered);				//设置输入点云
	pass.setFilterFieldName("y");			//设置过滤时所需要点云类型的Z字段
	pass.setFilterLimits(y1, y2);			//设置在过滤字段的范围
	pass.filter(*cloud_filtered1);          //执行滤波，保存过滤结果在cloud_filtered

	pass.setInputCloud(cloud_filtered1);	//设置输入点云
	pass.setFilterFieldName("x");			//设置过滤时所需要点云类型的x字段
	pass.setFilterLimits(x1, x2);			//设置在过滤字段的范围
	pass.filter(*cloud_filtered2);          //执行滤波，保存过滤结果在cloud_filtered

}

//删除墙面
bool Common::process_removePlane(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::PointCloud::Ptr &cloud_out, const float dis_threshold, const int max_iterations)
{
	// 对滤波之后的点云做平面分割，目的是确定场景中的平面，并得到平面的内点及其系数
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr plane_inliers(new pcl::PointIndices);
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	seg.setOptimizeCoefficients(true);
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(dis_threshold);
	seg.setMaxIterations(max_iterations);

	seg.setInputCloud(cloud_in);
	seg.segment(*plane_inliers, *coefficients);
	if (plane_inliers->indices.size() == 0) {
		return false;
	}
	std::cout << "inliers->indices.size = " << plane_inliers->indices.size() << std::endl;

	pcl::ExtractIndices<pcl::PointXYZ> ex;
	ex.setInputCloud(cloud_in);
	ex.setIndices(plane_inliers);
	ex.setNegative(true);
	ex.filter(*cloud_out);

	return true;
}
//中点移到原点
void Common::MoveCenTo000(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_in, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out)
{
	Eigen::Vector4f cloudCentroid;
	pcl::compute3DCentroid(*cloud_in, cloudCentroid);
	//计算点云质心 
	Eigen::Matrix4f translation = Eigen::Matrix4f::Identity();
	//定义平移矩阵，并初始化为单位阵 
	translation(0, 3) = -cloudCentroid[0];
	translation(1, 3) = -cloudCentroid[1];
	translation(2, 3) = -cloudCentroid[2];
	pcl::transformPointCloud(*cloud_in, *cloud_out, translation);
}

//旋转
void Common::RotateA(pcl::PointCloud<pcl::PointXYZ> input, double A, double B, double C, pcl::PointCloud<pcl::PointXYZ> &transformed_cloud)
{
	////计算平移距离
	//myPoinT min_p, max_p;
	//pcl::getMinMax3D(input, min_p, max_p);
	//float xx = max_p.x;
	//float yy = max_p.y;
	//float zz = max_p.z;
	//计算旋转角度 theta 正顺时针 负时逆时针
	//Eigen::Affine3f transform_M = Eigen::Affine3f::Identity();
	//transform_M.translation() << -xx, -yy, -zz;
	//pcl::transformPointCloud(X, Y, transform_M);

	pcl::PointCloud<pcl::PointXYZ> tempX;

	float thetaX = atan(-(C / B));
	Eigen::Affine3f transform_X = Eigen::Affine3f::Identity();
	transform_X.rotate(Eigen::AngleAxisf(thetaX, Eigen::Vector3f::UnitX()));
	pcl::transformPointCloud(input, tempX, transform_X);

	float thetaZ = -atan(-(A / B));
	Eigen::Affine3f transform_Z = Eigen::Affine3f::Identity();
	transform_Z.rotate(Eigen::AngleAxisf(thetaZ, Eigen::Vector3f::UnitZ()));
	pcl::transformPointCloud(tempX, transformed_cloud, transform_Z);
}

//旋转点云地面水平
void Common::process_estimatePlane(pcl::PointCloud<pcl::PointXYZ>::Ptr &in_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &out_cloud, const float dis_threshold, const int max_iterations, const int showcloud)
{
	if (0 == in_cloud->points.size()) {
		std::cout << "Cache Cloud Is Empty." << std::endl;
		return;
	}
	double degree = 30;

	pcl::PointIndices::Ptr plane_inliers(new pcl::PointIndices);
	Eigen::VectorXf coefficients;

	pcl::SampleConsensusModelPerpendicularPlane<pcl::PointXYZ>::Ptr model(new pcl::SampleConsensusModelPerpendicularPlane<pcl::PointXYZ>(in_cloud));
	model->setAxis(Eigen::Vector3f(0.0, 1.0, 0.0));	//setAxis用于设置所搜索平面垂直的轴
	model->setEpsAngle(pcl::deg2rad(degree));		//setEpsAngle用于设置待检测的平面模型和上述轴的最大角度
	pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(model);
	ransac.setMaxIterations(max_iterations);
	ransac.setDistanceThreshold(dis_threshold);
	ransac.computeModel();
	//获取内点
	ransac.getInliers(plane_inliers->indices);
	//获取平面参数
	ransac.getModelCoefficients(coefficients);
	std::cout << "平面参数 coefficients = " << coefficients << std::endl;
	std::cout << "平面内点 inliers->indices.size = " << plane_inliers->indices.size() << std::endl;

	////平移和旋转
	//PointCloud::Ptr rrPointC1(new PointCloud);
	//RotateA(*in_cloud, coefficients(0), coefficients(1), coefficients(2), *rrPointC1);
	////RotateB(*in_cloud, coefficients(0), coefficients(1), coefficients(2), *rrPointC1);
	//if (showcloud)
	//	ShowCloud2("estimatePlane rotate", in_cloud, rrPointC1);

	/********************************************************************************************/


	//地面
	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud(in_cloud);
	extract.setIndices(plane_inliers);
	pcl::PointCloud<pcl::PointXYZ>::Ptr plane(new pcl::PointCloud<pcl::PointXYZ>);
	extract.filter(*plane);
	if (showcloud)
		ShowCloud2("estimatePlane plane", plane, in_cloud);

	//去除地面
	extract.setNegative(true);
	extract.filter(*out_cloud);
}



//半径滤波器RadiusOutlierRemoval
void Common::filter_ROR(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out)
{

	// Create the filtering——RadiusOutlierRemoval  
	pcl::RadiusOutlierRemoval<pcl::PointXYZ> outrem;
	// build the filter 创建滤波器  
	outrem.setInputCloud(cloud);
	outrem.setRadiusSearch(500);//设置在500mm半径的范围内找邻近点  
	outrem.setMinNeighborsInRadius(50);//设置查询点的邻近点集数小于50的删除  
	// apply filter    应用滤波器  
	outrem.filter(*cloud_out);//执行条件滤波，存储结果到cloud_filtered 
}

//统计滤波器
void Common::filter_SOR(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_out)
{
	//创建滤波器，对每个点分析的临近点的个数设置为50 ，并将标准差的倍数设置为1  这意味着如果一
	//个点的距离超出了平均距离一个标准差以上，则该点被标记为离群点，并将它移除，存储起来
	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;		//创建滤波器对象
	sor.setInputCloud(cloud);							//设置待滤波的点云
	sor.setMeanK(100);									//设置在进行统计时考虑查询点临近点数
	sor.setStddevMulThresh(1.0);						//设置判断是否为离群点的阀值
	sor.filter(*cloud_out);								//执行滤波处理，存储输出


}


//计算点云的平均距离
float Common::avg_distance(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
	kdtree.setInputCloud(cloud);

	int k = 2;
	float avg_dis = 0;
	for (int i = 0; i < cloud->size() / 2; i++)
	{
		std::vector<int> nnh;
		std::vector<float> squaredistance;
		kdtree.nearestKSearch(cloud->points[i], k, nnh, squaredistance);
		avg_dis += sqrt(squaredistance[1]);
	}
	avg_dis = avg_dis / (cloud->size() / 2);
	return avg_dis;
}
//边界估计
void Common::estimateBorders(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_boundary, float reforn)
{
	pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>); //保存法线估计的结果 
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normEst; //定义一个法线估计的对象 
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	normEst.setSearchMethod(tree);
	normEst.setInputCloud(cloud);
	normEst.setRadiusSearch(reforn); //设置法线估计的半径 
	normEst.compute(*normals); //将法线估计结果保存至normals 


	//输出法线的个数 
	std::cout << "reforn: " << reforn << std::endl;
	std::cerr << "normals: " << normals->size() << std::endl;

	pcl::PointCloud<pcl::Boundary> boundaries; //保存边界估计结果 
	pcl::BoundaryEstimation<pcl::PointXYZ, pcl::Normal, pcl::Boundary> boundEst; //定义一个进行边界特征估计的对象 
	boundEst.setInputCloud(cloud); //设置输入的点云 
	boundEst.setInputNormals(normals); //设置边界估计的法线，因为边界估计依赖于法线 
#if 0
	boundEst.setRadiusSearch(re); //设置边界估计所需要的半径 
#else
	float avg_dis = avg_distance(cloud);
	boundEst.setRadiusSearch(avg_dis * 12); //设置边界估计所需要的半径 
	std::cout << "avg_dis: " << avg_dis << std::endl;
#endif
	//boundEst.setKSearch(50);
	boundEst.setAngleThreshold(M_PI / 4); //边界估计时的角度阈值 
	boundEst.setSearchMethod(pcl::search::KdTree<pcl::PointXYZ>::Ptr(new pcl::search::KdTree<pcl::PointXYZ>)); //设置搜索方式KdTree 
	boundEst.compute(boundaries); //将边界估计结果保存在boundaries 

	//输出边界点的个数 
	std::cerr << "boundaries: " << boundaries.points.size() << std::endl;
	//存储估计为边界的点云数据，将边界结果保存为pcl::PointXYZ类型 
	for (int i = 0; i < cloud->points.size(); i++)
	{
		if (boundaries[i].boundary_point > 0) {
			cloud_boundary->push_back(cloud->points[i]);
		}
	}


}

//估计法向量
void Common::estimateNormal(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
{
	// Create the normal estimation class, and pass the input dataset to it
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
	ne.setInputCloud(cloud);

	// Create an empty kdtree representation, and pass it to the normal estimation object.
	// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
	ne.setSearchMethod(tree);

	// Output datasets
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
	// Use all neighbors in a sphere of radius 50mm
	ne.setRadiusSearch(50);
	// Compute the features
	ne.compute(*cloud_normals);

	// cloud_normals->points.size () should have the same size as the input cloud->points.size ()*


	pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZ> fildColor(cloud, "z"); // 按照z字段进行渲染
	// visualize normals
	pcl::visualization::PCLVisualizer viewer("PCL Viewer");
	viewer.setBackgroundColor(0.0, 0.0, 0.5);
	viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(cloud, cloud_normals);
	//viewer.addPointCloud<myPoinT>(cloud, "sample cloud");
	viewer.addPointCloud<pcl::PointXYZ>(cloud, fildColor, "1");
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "1");

	while (!viewer.wasStopped())
	{
		viewer.spinOnce();
	}
}



//点云投影到XY平面
void Common::process_TransPInliers(pcl::PointCloud<pcl::PointXYZ>::Ptr & in, pcl::PointCloud<pcl::PointXYZ>::Ptr & out)
{
	//填充ModelCoefficients的值，本例中我们使用一个ax+by+cz+d=0的平面模型，其中a=b=d=0，c=1，换句话说，也就是X-Y平面
	//定义模型系数对象，并填充对应的数据
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
	coefficients->values.resize(4);
	coefficients->values[0] = coefficients->values[1] = 0; //a=b=0
	coefficients->values[2] = 1.0;//c=1
	coefficients->values[3] = 0;//d=0

	pcl::ProjectInliers<pcl::PointXYZ> proj;      //创建投影滤波对象
	proj.setModelType(pcl::SACMODEL_PLANE);      //设置对象对应的投影模型
	proj.setInputCloud(in);                    //设置输入点云
	proj.setModelCoefficients(coefficients);     //设置模型对应的系数
	proj.filter(*out);                //执行投影滤波存储结果out
}


//X-Y排序
void Common::PointCloudSortXY(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud)
{
	//X从小到大
	std::sort(cloud->begin(), cloud->end(),
		[](pcl::PointXYZ pt1, pcl::PointXYZ pt2) {return pt1.x < pt2.x; });
	//Y从小到大
	std::sort(cloud->begin(), cloud->end(),
		[](pcl::PointXYZ pt1, pcl::PointXYZ pt2) {return pt1.y < pt2.y; });
}





//算数平均滤波法
void Common::run_avg_fil(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, int k_or_radius)
{
	pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
	kdtree.setInputCloud(cloud);

	for (int i = 0; i < cloud->size(); i++)
	{
#if 0
		int k = k_or_radius;
		vector<int> k_idx;
		vector<float> squaredistance;
		k = kdtree.nearestKSearch(cloud->points[i], k, k_idx, squaredistance);
#else
		double radius = k_or_radius;
		std::vector<int> k_idx;
		std::vector<float> squaredistance;
		int k = kdtree.radiusSearch(cloud->points[i], radius, k_idx, squaredistance);
#endif

		if (k > 0) {
			pcl::PointXYZ p_sum(0, 0, 0);
			for (unsigned j = 0; j < k; j++) {
				pcl::PointXYZ &temp = cloud->at(k_idx[j]);
				p_sum.x += temp.x;
				p_sum.y += temp.y;
				p_sum.z += temp.z;
			}
			pcl::PointXYZ &point = cloud->at(i);
			point.x = p_sum.x / k;
			point.y = p_sum.y / k;
			point.z = p_sum.z / k;
		}
	}
}
/*
特征点名称		特征点位置与身高的比例
				男		女
头顶点			1. 00	1. 00
颈侧点			0. 84	0. 86
肩峰点			0. 82	0. 81
腋窝点			0. 75	0. 75
胸高点			0. 72	0. 72
腰侧点			0. 61	0. 63
臀凸点			0. 53	0. 53
会阴点			0. 47	0. 47
膝盖点			0. 26	0. 28
外踝点			0. 05	0. 05
脚点 　			0. 00	0. 00

人体特征位置高度比例（位置点比人体高度的高度比例）
特征位置  身高	肩		腋窝  胸围  肘		腰围  臀围  会阴  膝盖  脚踝
高度比例  1.0	0.82	0.75  0.72  0.63	0.62  0.58  0.47  0.27  0.05
*/

//获取某位置宽度
float GetYYWidth(pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud, float yy, float hh)
{
	bool first = true;
	float minXX = 0;
	float maxXX = 0;
	for (int i = 0; i < cloud->size(); i++)
	{
		pcl::PointXYZ &point = cloud->at(i);
		if (point.y >= yy - hh && point.y <= yy + hh) {
			if (first)
			{
				first = false;
				minXX = point.x;
				maxXX = point.x;
			}
			else {
				if (point.x < minXX)
					minXX = point.x;
				if (point.x > maxXX)
					maxXX = point.x;
			}
		}
	}

	return abs(maxXX - minXX);
}

//最终身高
void Common::BodyDate(pcl::PointCloud<pcl::PointXYZ>::Ptr & cloud) {
	float yy;
	pcl::PointXYZ min_p, max_p;
	pcl::getMinMax3D(*cloud, min_p, max_p);
	std::cout << min_p.y << std::endl;
	std::cout << max_p.y << std::endl;

	//"身高"
	float Height = abs(max_p.y - min_p.y) * 100 / 95;
	cout << "Height= " << Height << endl;

	//"肩宽"
	yy = max_p.y - Height * 0.17;
	float jk1 = GetYYWidth(cloud, yy, 15);
	yy = max_p.y - Height * 0.18;
	float jk2 = GetYYWidth(cloud, yy, 15);
	yy = max_p.y - Height * 0.19;
	float jk3 = GetYYWidth(cloud, yy, 15);
	float Shouder = _MIN3(jk1, jk2, jk3);
	cout << "Shouder= " << Shouder << endl;




}


void Common::savePCDFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud) {
	pcl::PCDWriter writer;
	writer.write("test.pcd", *cloud);
	//writer.write("PCD\\"+ std::to_string(cloud)+ ".pcd", *cloud);

}

void Common::savePLYFile(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud) {
	pcl::PCDWriter writer;
	//writer.write("PLY\\"+ toString(cloud).c_str()+".pcd", *cloud);
}
//点云数据转为vecotr
void PointCloud2Vector2d(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::on_nurbs::vector_vec2d &data)
{
	for (unsigned i = 0; i < cloud->size(); i++)
	{
		pcl::PointXYZ &p = cloud->at(i);
		if (!pcl_isnan(p.x) && !pcl_isnan(p.y))
			data.push_back(Eigen::Vector2d(p.x, p.y));
	}
}
void PointCloud2Vector3d(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::on_nurbs::vector_vec3d &data)
{
	for (unsigned i = 0; i < cloud->size(); i++)
	{
		pcl::PointXYZ &p = cloud->at(i);
		if (!pcl_isnan(p.x) && !pcl_isnan(p.y) && !pcl_isnan(p.z))
			data.push_back(Eigen::Vector3d(p.x, p.y, p.z));
	}
}
#if 0
void Common::run_curv_fitting(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud)
{
	pcl::visualization::PCLVisualizer viewer("B样条曲面拟合点云数据");
	viewer.setBackgroundColor(255, 255, 255);
	viewer.setSize(800, 600);

	//添加点云显示
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> handler(cloud, 0, 255, 0);
	viewer.addPointCloud<pcl::PointXYZ>(cloud, handler, "cloud_cylinder");

	// 数据类型转换：convert to NURBS data structure
	pcl::on_nurbs::NurbsDataSurface data;
	PointCloud2Vector3d(cloud, data.interior);
	// ############################################################################
	// ############################################################################
	// fit B-spline surface
	// parameters
	unsigned order(3);
	unsigned refinement(4);
	unsigned iterations(10);
	unsigned mesh_resolution(128);
	bool two_dim = true;

	pcl::on_nurbs::FittingSurface::Parameter params;
	params.interior_smoothness = 0.2;
	params.interior_weight = 1.0;
	params.boundary_smoothness = 0.2;
	params.boundary_weight = 0.0;

	// initialize
	printf("surface fitting ...\n");
	ON_NurbsSurface nurbs = pcl::on_nurbs::FittingSurface::initNurbsPCABoundingBox(order, &data);
	pcl::on_nurbs::FittingSurface fit(&data, nurbs);
	//fit.setQuiet (false); // enable/disable debug output

	// mesh for visualization
	/*pcl::PolygonMesh mesh;
	pcl::PointCloud<pcl::PointXYZ>::Ptr mesh_cloud(new pcl::PointCloud<pcl::PointXYZ>);
	std::vector<pcl::Vertices>mesh_vertices;
	std::string mesh_id ="mesh_nurbs";
	pcl::on_nurbs::Triangulation::convertSurface2PolygonMesh(fit.m_nurbs, mesh, mesh_resolution);
	viewer.addPolygonMesh(mesh, mesh_id);
	std::cout<<"Before refine" <<endl;*/
	//cout <<" click q key to quit the visualizer and continue!!" <<endl;
	//viewer.spin();
	// surface refinement 提炼
	//for (unsigned i =0; i <refinement; i++)
	//{
	//	fit.refine(0);
	//	if (two_dim) 
	//		fit.refine(1);
	//	fit.assemble(params);
	//	fit.solve();
	//	pcl::on_nurbs::Triangulation::convertSurface2Vertices(fit.m_nurbs, mesh_cloud, mesh_vertices, mesh_resolution);
	//	std::cout <<"refine: " <<i <<endl;
	//}
	//viewer.updatePolygonMesh<pcl::PointXYZ>(mesh_cloud, mesh_vertices, mesh_id);
	//std::cout <<"refines: " <<refinement <<endl;

	//// surface fitting with final refinement level
	//for (unsigned i =0; i <iterations; i++)
	//{
	//	fit.assemble(params);
	//	fit.solve();
	//	pcl::on_nurbs::Triangulation::convertSurface2Vertices(fit.m_nurbs, mesh_cloud, mesh_vertices, mesh_resolution);
	//	std::cout <<"iteration: " <<i <<endl;
	//}

	//viewer.updatePolygonMesh<pcl::PointXYZ>(mesh_cloud, mesh_vertices, mesh_id);
	//std::cout <<"surface fitting with iterations : " <<iterations <<endl;
	// ############################################################################
	// fit B-spline curve
	// parameters
	pcl::on_nurbs::FittingCurve2dAPDM::FitParameter curve_params;
	curve_params.addCPsAccuracy = 5e-2;
	curve_params.addCPsIteration = 3;
	curve_params.maxCPs = 200;
	curve_params.accuracy = 1e-3;
	curve_params.iterations = 100;

	curve_params.param.closest_point_resolution = 0;
	curve_params.param.closest_point_weight = 1.0;
	curve_params.param.closest_point_sigma2 = 0.1;
	curve_params.param.interior_sigma2 = 0.00001;
	curve_params.param.smooth_concavity = 1.0;
	curve_params.param.smoothness = 1.0;

	printf("curve fitting ...\n");
	pcl::on_nurbs::NurbsDataCurve2d curve_data;
	curve_data.interior = data.interior_param;
	curve_data.interior_weight_function.push_back(true);

	ON_NurbsCurve curve_nurbs = pcl::on_nurbs::FittingCurve2dAPDM::initNurbsCurve2D(order, curve_data.interior);
	pcl::on_nurbs::FittingCurve2dASDM curve_fit(&curve_data, curve_nurbs);
	curve_fit.fitting(curve_params);
	VisualizeCurve(viewer, curve_fit.m_nurbs, fit.m_nurbs, true);

	viewer.spin();
}

#endif