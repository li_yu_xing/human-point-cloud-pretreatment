#include<iostream>
#include<vector>
#include <cstring>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <boost/thread/thread.hpp>
#include <pcl/filters/normal_space.h> 
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/convolution_3d.h>
#include <pcl/filters/convolution.h>
#include <pcl/filters/impl/bilateral.hpp>
//Bilateral Filter  
#include <pcl/filters/bilateral.h> 
#include <pcl/filters/fast_bilateral.h>  
#include <pcl/filters/fast_bilateral_omp.h> 

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/sample_consensus/sac_model_perpendicular_plane.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/poisson.h>
#include <pcl/surface/mls.h>   //最小二乘平滑处理类定义
#include <pcl/features/normal_3d.h>
#include <pcl/features/boundary.h> 
#include <pcl/common/common.h>
#include <pcl/common/transforms.h> // 点云坐标变换
#include <pcl/common/angles.h>
#include <pcl/common/concatenate.h>
#include <pcl/keypoints/uniform_sampling.h> //均匀采样
#include <boost/thread/thread.hpp>


#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/point_types.h>
#include<pcl/io/ply_io.h>

#include<pcl/surface/on_nurbs/fitting_surface_tdm.h>