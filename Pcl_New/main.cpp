#include"Common.h"
G_CFG g_cfg;


int main()
{
	//pcl::PCDWriter writer;
	//************************读取点云文件*********************************************************
	std::cout << "点云读取文件模块" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::io::loadPLYFile("PointCloudData/pointcloud_add0.ply", *cloud);
	std::cout << cloud->points.size() << "points" << std::endl;
	/*if (pcl::io::loadPLYFile("PointCloudData/pointcloud_add3.ply", *cloud)) {
		cerr << "点云数据读取错误" << endl;
		return -1;
	}*/
	if (g_cfg.showcloud) {
		Common::ShowCloud("点云叠加文件", cloud);
	}
	std::cout << "点云读取文件模块结束" << std::endl;
	//**********************************************************************************************



#if 1
	//***************降采样滤波 体素滤波或均匀滤波**************************************************
	std::cout << "降采样滤波模块开始" << std::endl;
	//降采样滤波 体素滤波
	//PointCloud::Ptr cloud_tishu = Common::filter_VoxelGrid(cloud, g_cfg.LEAFSIZE);
	//Common::filter_VoxelGrid(cloud, cloud, g_cfg.LEAFSIZE);

	//降采样滤波 均匀采样
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ror = Common::filter_US(cloud, cloud,g_cfg.LEAFSIZE);
	Common::filter_US(cloud, cloud, g_cfg.LEAFSIZE);
	std::cout << cloud->points.size() << "points" << std::endl;

	if (g_cfg.showcloud) {
		Common::ShowCloud("降采样滤波", cloud);
		//Common::ShowCloud2("原点云和降采样滤波后点云", cloud, cloud_ror);
		pcl::io::savePCDFile("PCD/cloud.pcd", *cloud);
		pcl::io::savePLYFile("PLY/cloud.ply", *cloud);

	}
	std::cout << "降采样滤波模块结束" << std::endl;
	//***********************************************************************************************
#endif




#if 1
	//*************平滑处理**************************************************************************
	std::cout << "平滑处理开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pinghua(new pcl::PointCloud<pcl::PointXYZ>);
	std::cout << cloud->points.size() << std::endl;
	Common::process_mls(cloud, cloud_pinghua, g_cfg.RADIUS_MLS);
	std::cout << cloud_pinghua->points.size() << std::endl;
	if (g_cfg.showcloud) {
		//Common::ShowCloud("平滑处理", cloud_pinghua);
		Common::ShowCloud2("降采样滤波后点云,平滑处理", cloud, cloud_pinghua);

		pcl::io::savePCDFile("PCD/cloud_pinghua.pcd", *cloud_pinghua);
		pcl::io::savePLYFile("PLY/cloud_pinghua.ply", *cloud_pinghua);


	}

	std::cout << "平滑处理结束." << std::endl;
	//************************************************************************************************





	//*************************高斯滤波***************************************************************
	std::cout << "高斯滤波开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_gaoshi(new pcl::PointCloud<pcl::PointXYZ>);
	//Common::filter_Gaussian(cloud, cloud, g_cfg.RADIUS_GAUSSIAN);
	Common::filter_Gaussian(cloud_pinghua, cloud_gaoshi, g_cfg.RADIUS_GAUSSIAN);

	if (g_cfg.showcloud) {
		Common::ShowCloud("高斯滤波", cloud_gaoshi);
		Common::ShowCloud2("降采样滤波后点云,高斯滤波", cloud, cloud_gaoshi);

		//Common::ShowCloud2("原点云和降采样滤波后点云", cloud, cloud_ror);
		pcl::io::savePCDFile("PCD/cloud_gaoshi.pcd", *cloud_gaoshi);
		pcl::io::savePLYFile("PLY/cloud_gaoshi.ply", *cloud_gaoshi);
	}

	std::cout << "高斯滤波结束" << std::endl;
	//************************************************************************************************

#endif


	////*************************双边滤波***************************************************************
	//std::cout << "双边滤波开始" << std::endl;
	//PointCloud::Ptr cloud_bi(new PointCloud);
	//Common::filter_bilateral2(cloud_ga, cloud_bi,100, 0.1);
	//if (g_cfg.showcloud)
	//	//Common::ShowCloud("双边滤波", cloud_bi);
	//std::cout << "双边滤波结束" << std::endl;
	////************************************************************************************************






	//**********************直通滤波******************************************************************
	std::cout << "直通滤波开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_zhitong(new pcl::PointCloud<pcl::PointXYZ>);
	Common::filter_pass_through(cloud_gaoshi, cloud_zhitong, g_cfg.rang_z1, g_cfg.rang_z2, g_cfg.rang_x1, g_cfg.rang_x2, g_cfg.rang_y1, g_cfg.rang_y2);
	if (g_cfg.showcloud) {
		Common::ShowCloud("直通滤波", cloud_zhitong);
		Common::ShowCloud2("平滑处理", cloud, cloud_zhitong);
		pcl::io::savePCDFile("PCD/cloud_zhitong.pcd", *cloud_zhitong);
		pcl::io::savePLYFile("PLY/cloud_zhitong.ply", *cloud_zhitong);

	}

	std::cout << "直通滤波结束" << std::endl;
	//************************************************************************************************

	/*
	//************中点移到原点*************************************************************************
	std::cout << "中点移到原点开始" << std::endl;
	PointCloud::Ptr cloud_remove_center_point(new PointCloud);
	Common::MoveCenTo000(cloud_removewall, cloud_remove_center_point);
	if (g_cfg.showcloud) {
		Common::ShowCloud("删除墙面Wall2", cloud_remove_center_point);
	}
	std::cout << "中点移到原点结束" << std::endl;
	//*************************************************************************************************
	*/






	//***********************点云地面处理**************************************************************
	std::cout << "点云地面处理开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_dimian(new pcl::PointCloud<pcl::PointXYZ>);
	Common::process_estimatePlane(cloud_zhitong, cloud_dimian, g_cfg.dis_threshold2, g_cfg.max_iterations2, g_cfg.showcloud);
	if (g_cfg.showcloud) {
		//Common::ShowCloud("删除地面Wall2", cloud_dimian);
		Common::ShowCloud2("删除地面Wall2", cloud_zhitong, cloud_dimian);


		pcl::io::savePCDFile("PCD/cloud_dimian.pcd", *cloud_dimian);
		pcl::io::savePLYFile("PLY/cloud_dimian.ply", *cloud_dimian);

	}

	std::cout << "点云地面处理结束" << std::endl;
	//*************************************************************************************************


#if 0
	//*****************************半径滤波器 统计滤波器***********************************************
	//统计滤波器
	std::cout << "半径滤波 统计滤波器开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tongji(new pcl::PointCloud<pcl::PointXYZ>);
	Common::filter_SOR(cloud_dimian, cloud_tongji);
	if (g_cfg.showcloud)
		//Common::ShowCloud("统计滤波器之后", cloud_tongji);

		pcl::io::savePCDFile("PCD/cloud_tongji.pcd", *cloud_tongji);
	pcl::io::savePLYFile("PLY/cloud_tongji.ply", *cloud_tongji);


#endif // 0

	//半径滤波器
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_banjing(new pcl::PointCloud<pcl::PointXYZ>);
	Common::filter_ROR(cloud_dimian, cloud_banjing);
	if (g_cfg.showcloud)
		Common::ShowCloud("半径滤波器", cloud_banjing);
	//Common::ShowCloud2("统计滤波器 半径滤波", cloud_tongji, cloud_banjing);

	//pcl::io::savePCDFile("PCD/cloud_banjing.pcd", *cloud_banjing);
	//pcl::io::savePLYFile("PLY/cloud_banjing.ply", *cloud_banjing);


	std::cout << "半径滤波 统计滤波器结束" << std::endl;

	//*************************************************************************************************

	//**************************点云边界平滑***********************************************************
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pinghuua2(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pinghuua3(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pinghuua4(new pcl::PointCloud<pcl::PointXYZ>);

	Common::process_mls(cloud_banjing, cloud_pinghuua2, 500);
	Common::process_mls(cloud_banjing, cloud_pinghuua3, 1000);
	Common::process_mls(cloud_banjing, cloud_pinghuua3, 1000);
	Common::process_mls(cloud_banjing, cloud_pinghuua4, 2000);


	Common::ShowCloud2("半径滤波 平滑", cloud_banjing, cloud_pinghuua2);
	Common::ShowCloud2("半径滤波 平滑2", cloud_pinghuua2, cloud_pinghuua3);
	Common::ShowCloud2("半径滤波 平滑3", cloud_pinghuua3, cloud_pinghuua4);







	//*************************************************************************************************


	//********************************边界估计*********************************************************
	std::cout << "边界估计开始" << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_body_borders(new pcl::PointCloud<pcl::PointXYZ>);
	Common::estimateBorders(cloud_banjing, cloud_body_borders, 100);
	if (g_cfg.showcloud) {
		Common::ShowCloud("边界估计", cloud_body_borders);
		pcl::io::savePCDFile("PCD/cloud_body_borders.pcd", *cloud_body_borders);
		pcl::io::savePLYFile("PLY/cloud_body_borders.ply", *cloud_body_borders);


	}

	std::cout << "边界估计结束" << std::endl;
	//*************************************************************************************************






	//**********************点云投影到XY平面********X-Y排序************算数平均滤波法******************

	//点云投影到XY平面
	std::cout << "点云投影到XY平面..." << std::endl;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_body_borders_xy(new pcl::PointCloud<pcl::PointXYZ>);
	Common::process_TransPInliers(cloud_body_borders, cloud_body_borders_xy);
	if (g_cfg.showcloud) {
		Common::ShowCloud("点云投影到XY平面", cloud_body_borders_xy);

		pcl::io::savePCDFile("PCD/cloud_body_borders_xy.pcd", *cloud_body_borders_xy);
		pcl::io::savePLYFile("PLY/cloud_body_borders_xy.ply", *cloud_body_borders_xy);
	}

	//X-Y排序
	Common::PointCloudSortXY(cloud_body_borders_xy);
	if (g_cfg.showcloud) {
		Common::ShowCloud("X-Y排序", cloud_body_borders_xy);
		Common::ShowCloud2("边界估计 X-Y排序", cloud_body_borders, cloud_body_borders_xy);

		pcl::io::savePCDFile("PCD/cloud_body_borders_xy2.pcd", *cloud_body_borders_xy);
		pcl::io::savePLYFile("PLY/cloud_body_borders_xy2.ply", *cloud_body_borders_xy);


	}

	//中心排序
	//CCommon::PointCloudSortCen(cloud_out);
	//算数平均滤波法
	Common::run_avg_fil(cloud_body_borders_xy, 10);
	if (g_cfg.showcloud) {
		Common::ShowCloud("算数平均滤波法", cloud_body_borders_xy);
		Common::ShowCloud2("边界估计 X-Y排序", cloud_body_borders, cloud_body_borders_xy);

		pcl::io::savePCDFile("PCD/cloud_body_borders_xy3.pcd", *cloud_body_borders_xy);
		pcl::io::savePLYFile("PLY/cloud_body_borders_xy3.ply", *cloud_body_borders_xy);


	}

	//*************************************************************************************************

	/**************************************************************************************************

	//曲线拟合
	//UsingCurveFitting::run_curv_fitting(cloud_out, UsingCurveFitting::TYPE_DEFAULT);

	**************************************************************************************************/



	//**********************计算身高*******************************************************************
	Common::BodyDate(cloud_body_borders_xy);
	//*************************************************************************************************


	return 0;
}
