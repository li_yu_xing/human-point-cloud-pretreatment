#include <boost/thread/thread.hpp>
#include <iostream>              //标准C++库中的输入输出的头文件
#include <pcl/io/pcd_io.h>           //PCD读写类相关的头文件
#include <pcl/point_types.h>      //PCL中支持的点类型的头文件
#include <pcl/surface/mls.h>   //最小二乘平滑处理类定义

void process_mls(pcl::PointCloud<pcl::PointXYZ>::Ptr & in, pcl::PointCloud<pcl::PointXYZ>::Ptr & out, double radius)
{
	//创建搜索树
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
	tree->setInputCloud(in);

	//最小二乘
	pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;//定义最小二乘对象
	mls.setComputeNormals(false);	//设置最小二乘计算是否需要存储计算的法线
	mls.setInputCloud(in);			//设置输入点云
	mls.setPolynomialOrder(2);		//2阶多项式
	mls.setPolynomialFit(false);	//设置false 可以加速smooth
	mls.setSearchMethod(tree);		//设置kdTree作为搜索方法
	mls.setSearchRadius(radius);	//拟合的k临近半径
	mls.process(*out);
}

int main(int argc, char** argv) {
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::io::loadPCDFile("PointCloudData/pointcloud_add0.pcd", *cloud);

	return 0;
}