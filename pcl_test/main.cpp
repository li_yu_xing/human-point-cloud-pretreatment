/*
 * @File: uniform_sample.cpp
 * @Brief: pcl course
 * @Description: 展示UniformSample均匀采样效果
 * @Version: 0.0.1
 * @Author: MuYv
 */
#include <iostream>
#include <string>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/visualization/pcl_visualizer.h>



int main(int argc, char** argv) {
	if (argc != 2) {
		std::cout << "Usage: exec cloud_file_path" << std::endl;
		return -1;
	}
	const std::string kCloudFilePath = argv[1];     // ../clouds/cabinet/cabinet_color_cloud.pcd

	// 定义变量
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_src(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZRGB>());

	// 成功返回0，失败返回-1
	if (-1 == pcl::io::loadPCDFile(kCloudFilePath, *cloud_src)) {
		std::cout << "load pcd file failed. please check it." << std::endl;
		return -2;
	}
	cout << "->加载了 " << cloud_src->points.size() << " 个数据点" << endl;
	// 创建均匀采样（模板）类对象，点数据类型为 pcl::PointXYZRGB
	pcl::UniformSampling<pcl::PointXYZRGB> us;
	// 设置输入点云，注意：此处传入的是点云类对象的智能指针
	us.setInputCloud(cloud_src);
	// 设置采样半径，数值越大越稀疏
	us.setRadiusSearch(30.0f);
	// 执行过滤，并带出处理后的点云数据，注意：此处传入的是点云类对象
	us.filter(*cloud_filtered);
	cout << "->加载了 " << cloud_filtered->points.size() << " 个数据点" << endl;

	// 创建可视化对象
	pcl::visualization::PCLVisualizer viewer("viewer");

	// 将当前窗口，拆分成横向的2个可视化窗口，以viewport区分(v1/v2)
	int v1;
	int v2;
	//窗口参数分别对应 x_min, y_min, x_max, y_max, viewport
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.createViewPort(0.5, 0.0, 1.0, 1.0, v2);

	// 添加2d文字标签
	viewer.addText("v1", 10, 10, 20, 1, 0, 0, "viewport_v1", v1);
	viewer.addText("v2", 10, 10, 20, 0, 1, 0, "viewport_v2", v2);

	// 添加坐标系
	viewer.addCoordinateSystem(0.5);    // 单位：m

	// 设置可视化窗口背景色
	viewer.setBackgroundColor(0.2, 0.2, 0.2);     // r,g,b  0~1之间

	// 向v1窗口中添加点云
	viewer.addPointCloud(cloud_src, "cloud_src", v1);
	// 根据点云id，设置点云可视化属性，此处将可视化窗口中的点大小调整为2级
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "cloud_src");

	// 向v2窗口中添加点云
	viewer.addPointCloud(cloud_filtered, "cloud_filtered", v2);
	// 根据点云id，设置点云可视化属性，此处将可视化窗口中的点大小调整为2级
	viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "cloud_filtered");

	// 关闭窗口则退出
	while (!viewer.wasStopped()) {
		viewer.spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}

	return 0;
}
